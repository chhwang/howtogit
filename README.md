# Starting a new project
###### Set who am I
    git config --global user.name "Changho Hwang"
    git config --global user.email "ch.hwang128@gmail.com"

###### Make the first commit of the project
    cd project_folder
    git init
    git remote add origin https://gitlab.com/chhwang/project.git
    git add first_file_to_commit
    git commit
    git push -u origin master


# Developing project
###### Change repository URL
    git remote set-url origin https://gitlab.com/chhwang/new_project.git

###### Discard all changes and commits, go back to a specific commit
    git reset --hard 0d1d7fc32

###### Revert the most recent commit
    git revert HEAD

###### Discard all local changes and commits and synch with the remote
    git fetch
    git reset --hard
    git pull origin branch_name

###### Temporarily go back to an old commit
    git checkout 0d1d7fc32

###### Checkout only one file
    git fetch
    git checkout 0d1d7fc32 -- file_to_checkout

###### Merge branches as one of them
    git checkout better_branch
    git merge -s ours another_branch
    git checkout another_branch
    git merge better_branch

###### Delete remote branch
    git push origin --delete branch_name


# Checking Log
###### Pretty log with graph
    git log --pretty=format:"%h %s" --graph

###### Using grep
    git log --grep="blahblah"

###### Print log stat
    git log --stat


# Managing Repository
###### List files in repository history
    git rev-list --objects --all | git cat-file --batch-check='%(objecttype) %(objectname) %(objectsize) %(rest)' | awk '/^blob/ {print substr($0,6)}' | sort --numeric-sort --key=2 | cut --complement --characters=8-40 | numfmt --field=2 --to=iec-i --suffix=B --padding=7 --round=nearest

###### Figure out which commit modified a file
    git log --pretty=oneline --all -- file_path

###### Clean a file from history
    git filter-branch --tag-name-filter cat --index-filter 'git rm -r --cached --ignore-unmatch filename' --prune-empty -f -- --all

###### Reclaim local disk space
    rm -rf .git/refs/original/
    git reflog expire --expire=now --all
    git gc --prune=now
    git gc --aggressive --prune=now

###### Push cleaned repository
    git push origin --force --all
    git push origin --force --tags

